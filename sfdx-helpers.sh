#!/bin/bash

function authenticate() {
  echo authenticate started for $1 ! 
  sf org login jwt -i $2 -f assets/server.key --username $1 --alias dev-org --set-default --set-default-dev-hub --json
}

function create_scratch_org(){
  local scratch_alias=$1
  sf org create scratch --definition-file config/project-scratch-def.json --set-default --alias $scratch_alias
}

function run_test_deploy(){
  sf project deploy start --metadata ApexClass --test-level RunLocalTests
}

function delete_scratch_org(){
  local scratch_alias=$1
  sf org delete scratch --target-org $scratch_alias --no-prompt
}

function unit_test_verify(){
  create_scratch_org $1
  run_test_deploy
  delete_scratch_org $1
}

function install_node(){
  apt-get update && apt-get install -y curl git build-essential libssl-dev
  curl -o- https://raw.githubusercontent.com/nvm-sh/nvm/v0.39.7/install.sh | bash
  source ~/.nvm/nvm.sh
  nvm install 20
  node -v
  npm -v
}

function install_dependency(){
  apt-get update -y && apt-get install wget -y && apt-get -y install unzip
  install_java
  install_node
  npm install @salesforce/cli --global
  echo 'y' | sfdx plugins:install sfdx-git-delta
  npm install eslint @babel/core @babel/eslint-parser @lwc/eslint-plugin-lwc @salesforce/eslint-plugin-aura eslint-plugin-jsdoc@latest --save-dev
  npm install --save-dev @salesforce/eslint-config-lwc @lwc/eslint-plugin-lwc @salesforce/eslint-plugin-lightning eslint-plugin-import eslint-plugin-jest
  install_pmd
  install_prettier
}

function install_prettier() {
  npm install --save-dev --save-exact prettier
  npm install --save-dev --save-exact prettier prettier-plugin-apex
}

function check_prettier() {
  npm run prettier:check
}

function install_java(){
  apt-get install -y openjdk-11-jdk ant
  java -version
}

function install_pmd(){
  wget https://github.com/pmd/pmd/releases/download/pmd_releases%2F7.1.0/pmd-dist-7.1.0-bin.zip
  unzip pmd-dist-7.1.0-bin.zip
}

function run_pmd(){
  java -version
  pmd-bin-7.1.0/bin/pmd check -d force-app -R config/custom-apex-rules.xml -f text
}

function run_eslint(){
  npm run lint:lwc
}

function create_package(){
  local SOURCE_BRANCH= $1
  echo $1
  local LAST_DEPLOYED_COMMIT=$(git show-ref -s $SOURCE_BRANCH)
  echo $LAST_DEPLOYED_COMMIT
  mv force-app force-app-mdapi
  mkdir force-app
  sfdx sgd:source:delta -t "HEAD" -f "HEAD~1" --output .
  cd force-app
  ls
}